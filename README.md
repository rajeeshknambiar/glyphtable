# GlyphTable

Generates a table with names and images of the glyphs in a font.
A TeX file is generated, which needs to be manually compiled (twice)
to generate the output PDF.

# Usage
`glyphtable -f <path_to_font> [-c cols]`

Optional columns could be specified to control the number of columns
in resulting PDF/TeX file.

# Requirements
- python3
- fontforge
- xelatex


# License
GPLv3
