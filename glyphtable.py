#!/usr/bin/python
# vim: noexpandtab ts=4 sts=4 sw=4
#
# Generate TeX source for a table of glyph names and images in a font.
# Requires fontforge, and pdflatex to compile resulting TeX file
#
# Copyright 2020 Rajeesh KV <rajeeshknambiar@gmail.com>
# License: GPLv3

import os, shutil, tempfile
import argparse
import fontforge as ff

def filterMalayalamGlyphs(g):   #'g' of type fontforge.glyph
	#Return glyphs for Malayalam, filtering out Latin, notdef/null, oldstyle figures and reference glyphs
	return ((g.unicode == -1 or g.unicode >= 0x0D00) and \
		g.glyphname.find('oldstyle') == -1  and g.glyphname.find('null') == -1 \
		and g.glyphname.find('NULL') == -1 and g.glyphname.find('notdef') == -1 \
		and g.glyphname.find('.ref1') == -1)

# This function could use some refactoring...
def GenerateGlyphTable(fontpath, num_cols=7):
	num_cols = int(num_cols)
	f = ff.open(fontpath)

	if not f:
		print("Couldn't open font, please verify the path")
		return
	else:
		tempdir = tempfile.mkdtemp(prefix="font")
		if not tempdir:
			print("Couldn't create temporarty directory, quitting")
			return

	if num_cols <= 1:
		num_cols = 7

	TEXFILE = os.path.join(tempdir, 'glyphtable.tex')
	texfile = open(TEXFILE, 'wt')
	if not texfile:
		print("Couldn't create texfile, quitting")
		return

	texfile.write('''\\documentclass{article}
	\\usepackage{longtable}
	\\usepackage[margin=1.2cm]{geometry}
	\\usepackage{graphicx}
	\\usepackage{xcolor}
	\\usepackage{fontspec}\n''')
	texfile.write('\\setmainfont[Script=Malayalam]{' + os.path.basename(fontpath) +\
			'}[Path=' + os.path.dirname(os.path.abspath(fontpath)) + '/]')
	texfile.write('\n\n\\begin{document}\n\n Glyphs from: ' + f.fontname + '\n')
	# longtable with num_cols columns
	texfile.write('\\begin{longtable}{|*{' + str(num_cols) + '}{c|}} \\hline\n')

	hdrline = ""
	imgline = ""

	#for i,g in enumerate(filter(filterMalayalamGlyphs, f.glyphs())):
	for i,g in enumerate(f.glyphs()):
		san_glyphname = g.glyphname.replace('.','_')
		glyphfilename = os.path.join(tempdir, san_glyphname + '.png')
		#g.export(glyphfilename)
		hdrline += '\\color{gray}{' + g.glyphname.replace('_','\_') + '} &'
		#imgline += '\\includegraphics[height=1.0cm]{' + san_glyphname + '} &'
		imgline += '{\\Huge\\XeTeXglyph ' + str(i) + '} &'
		if (i+1)%(num_cols-1) == 0:		#Need i+1 to due to i starting at 0, and num_cols-1 to limit columns
			texfile.write(hdrline.rstrip('&') + '\\\\\\nopagebreak\n')
			texfile.write(imgline.rstrip('&') + '\\\\ \\hline\n')
			hdrline = ""
			imgline = ""
	if hdrline:
		texfile.write(hdrline.rstrip('&') + '\\\\\n')
		texfile.write(imgline.rstrip('&') + '\\\\ \\hline\n')
	texfile.write('\\end{longtable}\n\n\\end{document}')
	texfile.close()
	print("Please compile " + TEXFILE + " using xelatex twice to generate the glyphs table")


if __name__ == "__main__":
	ap = argparse.ArgumentParser()
	ap.add_argument("-f", help="Path to font (sfd,ttf,otf,woff)")
	ap.add_argument("-c", help="Number of columns in output table")
	
	args = ap.parse_args()
	fontloc = args.f if args.f else None
	num_cols = args.c if args.c else 7
	GenerateGlyphTable(fontloc, num_cols)
